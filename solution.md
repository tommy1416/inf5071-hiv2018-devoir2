# Solution au devoir 2

## Auteur

Indiquez ici votre prénom, votre nom et votre code permanent

## Solution à la question 1

Indiquez ici toute explication ou justification qui permet d'appuyer la réponse
que vous avez fournie pour la question 1. Référez au fichier qui contient votre
solution, par exemple [q1.py](q1.py). S'il y en a d'autres, n'hésitez pas à y
référer et à les expliquer brièvement. Attention aux fautes d'orthographe.
Notez qu'il n'est pas obligatoire de donner des explications très détaillées,
mais il faut minimalement indiquer la ou les commandes à entrer (et le résultat
attendu) pour vérifier que votre programme fonctionne bien.

## Solution à la question 2

#define PI 3.14159265
#define A 20.0

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
    vec2 center = iResolution.xy / 2.0;
    vec2 dist = center - fragCoord;
    float r = length(dist) ;
    float theta = atan(dist.y, dist.x) / (2.0 * PI) + iTime;
    r = r - A * theta;
    float c = r / A - (floor(r / A));
    fragColor = vec4(smoothstep(0.2, 0.0, abs(c - 0.25)));
}



## Dépendances

Indiquez ici toutes les dépendances pour faire fonctionner votre projet. Une
dépendance est un logiciel ou une bibliothèque qui doit être installée pour
pouvoir reproduire les commandes et les résultats que vous obtenez.

## Références

Indiquez ici toute référence qui vous a permis de résoudre une ou plusieurs
questions.

## État du devoir

Indiquez ici si le devoir est complété, ou s'il y a certaines questions pour
lesquelles aucune réponse n'est fournie (ou simplement une réponse partielle).
